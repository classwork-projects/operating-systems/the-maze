//
//  main.c
//  Final Project CSC412
//
#include <iostream>
#include <string>
#include <vector>
//
#include <cstdio>
#include <cstdlib>
#include <ctime>
//
#include <pthread.h>
#include <unistd.h>
#include <tuple>
#include <algorithm>
#include <functional>
//
#include "gl_frontEnd.h"


using namespace std;

//==================================================================================
//	Function prototypes
//==================================================================================
void initializeApplication(void);
GridPosition getNewFreePosition(void);
Direction newDirection(Direction forbiddenDir = NUM_DIRECTIONS);
TravelerSegment newTravelerSeg(const TravelerSegment& currentSeg, bool& canAdd);
void generateWalls(void);
void generatePartitions(void);
void* findExit(void *currentThreadInfo);


// Thread info for traveler threads
using ThreadInfo = struct {
    pthread_t               threadID;
    unsigned int            index;
    vector<Traveler>*       travelers;
    bool                    stuck;
    std::string             active;
    pthread_mutex_t         travelerLock;
};

//==================================================================================
//	Application-level global variables
//==================================================================================

//	Don't rename any of these variables
//-------------------------------------
//	The state grid and its dimensions (arguments to the program)
SquareType** grid;
unsigned int numRows = 0;	//	height of the grid
unsigned int numCols = 0;	//	width
unsigned int numMoves = 4294967295;//Initialize to uint max
unsigned int numTravelers = 0;	//	initial number
unsigned int numTravelersDone = 0;
unsigned int numLiveThreads = 0;		//	the number of live traveler threads
vector<Traveler> travelerList;
vector<SlidingPartition> partitionList;
GridPosition	exitPos;	//	location of the exit

//	robot sleep time between moves (in microseconds)
const int MIN_SLEEP_TIME = 1000;
int travelerSleepTime = 100000;

//	An array of C-string where you can store things you want displayed
//	in the state pane to display (for debugging purposes?)
//	Dont change the dimensions as this may break the front end
const int MAX_NUM_MESSAGES = 8;
const int MAX_LENGTH_MESSAGE = 32;
char** message;
time_t launchTime;


std::vector<ThreadInfo> threadInfo;
// mutex lock to control access to the grid
pthread_mutex_t gridLock;
//column and row locks
std::vector<pthread_mutex_t> rowLocks;
std::vector<pthread_mutex_t> colLocks;

// state of program
bool running = true;

//==================================================================================
//	These are the functions that tie the simulation with the rendering.
//	Some parts are "don't touch."  Other parts need your intervention
//	to make sure that access to critical section is properly synchronized
//==================================================================================

void drawTravelers(void)
{
    //-----------------------------
    //	You may have to sychronize things here
    //-----------------------------
    for (unsigned int k=0; k<travelerList.size(); k++)
    {
	    pthread_mutex_lock(&threadInfo[k].travelerLock);
	    std::cout<<"1: Thread "<< k <<" locked"<<std::endl;
	    //	join inactive threads
	    if (threadInfo[k].active == "DONE")
	    {
		    pthread_join(threadInfo[k].threadID, NULL);
		    threadInfo[k].active = "JOINED";
		    printf("Thread %d joined.\n", k);
	    }
        drawTraveler(travelerList[k]);
	    pthread_mutex_unlock(&threadInfo[k].travelerLock);
	    std::cout<<"2: Thread "<< k <<" unlocked"<<std::endl;

    }
}

void updateMessages(void)
{
    //	Here I hard-code a few messages that I want to see displayed
    //	in my state pane.  The number of live robot threads will
    //	always get displayed.  No need to pass a message about it.
    unsigned int numMessages = 4;
    sprintf(message[0], "We created %d travelers", numTravelers);
    sprintf(message[1], "%d travelers solved the maze", numTravelersDone);
    sprintf(message[2], "I like cheese");
    sprintf(message[3], "Simulation run time is %ld", time(NULL)-launchTime);

    //---------------------------------------------------------
    //	This is the call that makes OpenGL render information
    //	about the state of the simulation.
    //
    //	You *must* synchronize this call.
    //
    //---------------------------------------------------------
    drawMessages(numMessages, message);
}

void handleKeyboardEvent(unsigned char c, int x, int y)
{
    int ok = 0;

    switch (c)
    {
        //	'esc' to quit
        case 27:
            running = false;
		    // wait for all threads to return
		    for (int i = 0; i < threadInfo.size(); i++)
		    {
			    pthread_mutex_lock(&threadInfo[i].travelerLock);
			    while (threadInfo[i].active == "RUNNING")
			    {
				    pthread_mutex_unlock(&threadInfo[i].travelerLock);
					usleep(travelerSleepTime*3);
				    pthread_mutex_lock(&threadInfo[i].travelerLock);
			    }
		    }
            //	Join threads
            for (unsigned i = 0; i < threadInfo.size(); i++)
            {
	            pthread_mutex_lock(&threadInfo[i].travelerLock);
	            std::cout<<"3: Thread "<< i <<" locked"<<std::endl;
	            if (threadInfo[i].active == "DONE") {
                    pthread_join(threadInfo[i].threadID, NULL);
                    threadInfo[i].active = "JOINED";
                    printf("Thread %d joined.\n", i);
                }
	            pthread_mutex_unlock(&threadInfo[i].travelerLock);
	            std::cout<<"4: Thread "<< i <<" unlocked"<<std::endl;
            }
            numLiveThreads = 0;
            exit(0);
            break;

            //	slowdown
        case ',':
            slowdownTravelers();
            ok = 1;
            break;

            //	speedup
        case '.':
            speedupTravelers();
            ok = 1;
            break;

        default:
            ok = 1;
            break;
    }
    if (!ok)
    {
        std::cout << "Error processing keyboard input, terminating program." << std::endl;
        for (unsigned i = 0; i < numTravelers; i++)
        {
            pthread_join(threadInfo[i].threadID, NULL);
            printf("Thread %d joined.\n", i);
        }

        exit(0);
    }
}


//------------------------------------------------------------------------
//	You shouldn't have to touch this one.  Definitely if you don't
//	add the "producer" threads, and probably not even if you do.
//------------------------------------------------------------------------
void speedupTravelers(void)
{
    //	decrease sleep time by 20%, but don't get too small
    int newSleepTime = (8 * travelerSleepTime) / 10;

    if (newSleepTime > MIN_SLEEP_TIME)
    {
        travelerSleepTime = newSleepTime;
    }
}

void slowdownTravelers(void)
{
    //	increase sleep time by 20%
    travelerSleepTime = (12 * travelerSleepTime) / 10;
}




//------------------------------------------------------------------------
//	You shouldn't have to change anything in the main function besides
//	initialization of the various global variables and lists
//------------------------------------------------------------------------
int main(int argc, char** argv)
{
    //	We know that the arguments  of the program  are going
    //	to be the width (number of columns) and height (number of rows) of the
    //	grid, the number of travelers, etc.
    //	So far, I hard code-some values

    //Invalid number of arguments
    if(argc < 4 || argc > 5)
    {
        std::cout<<"Invalid number of arguments\n";
        exit(1);
    }

    //14*10 is smallest grid
    //grid width
    numCols = atoi(argv[1]);
    //Minimum grid dimensions
    if(numCols < 14)
    {
        std::cout<<"Number of columns too small\n";
        exit(1);
    }

    //grid height
    numRows = atoi(argv[2]);
    if(numRows < 10)
    {
        std::cout<<"Number of rows too small\n";
        exit(1);
    }

    numTravelers = atoi(argv[3]);

    //moves to grow (not entered defaults to UINT_MAX)
    if(argc == 5)
    {
        numMoves = atoi(argv[4]);
    }

    numLiveThreads = 0;
    numTravelersDone = 0;
    // reserve memory to make push_backs less costly
    threadInfo.reserve(numTravelers);
    // mutex lock to control access to the grid
    pthread_mutex_init(&gridLock, NULL);
	// column and row mutex locks
	for(unsigned int i = 0; i < numCols; i++)
	{
		pthread_mutex_t lock;
		colLocks.push_back(lock);
		pthread_mutex_init(&colLocks[i], NULL);
	}
	for(unsigned int i = 0; i < numRows; i++)
	{
		pthread_mutex_t lock;
		rowLocks.push_back(lock);
		pthread_mutex_init(&rowLocks[i], NULL);
	}
    //	Even though we extracted the relevant information from the argument
    //	list, I still need to pass argc and argv to the front-end init
    //	function because that function passes them to glutInit, the required call
    //	to the initialization of the glut library.
    initializeFrontEnd(argc, argv);

    //	Now we can do application-level initialization
    initializeApplication();

    launchTime = time(NULL);

    //	Now we enter the main loop of the program and to a large extend
    //	"lose control" over its execution.  The callback functions that
    //	we set up earlier will be called when the corresponding event
    //	occurs

    //	Free allocated resource before leaving (not absolutely needed, but
    //	just nicer.  Also, if you crash there, you know something is wrong
    //	in your code.
    for (unsigned int i=0; i< numRows; i++)
        free(grid[i]);
    free(grid);
    for (int k=0; k<MAX_NUM_MESSAGES; k++)
        free(message[k]);
    free(message);

    //	This will probably never be executed (the exit point will be in one of the
    //	call back functions).
    return 0;
}


//==================================================================================
//
//	This is a function that you have to edit and add to.
//
//==================================================================================


void initializeApplication(void)
{
    //	Allocate the grid
    grid = new SquareType*[numRows];
    for (unsigned int i=0; i<numRows; i++)
    {
        grid[i] = new SquareType[numCols];
        for (unsigned int j=0; j< numCols; j++)
            grid[i][j] = FREE_SQUARE;

    }

    message = (char**) malloc(MAX_NUM_MESSAGES*sizeof(char*));
    for (unsigned int k=0; k<MAX_NUM_MESSAGES; k++)
        message[k] = (char*) malloc((MAX_LENGTH_MESSAGE+1)*sizeof(char));

    //---------------------------------------------------------------
    //	All the code below to be replaced/removed
    //	I initialize the grid's pixels to have something to look at
    //---------------------------------------------------------------
    //	Yes, I am using the C random generator after ranting in class that the C random
    //	generator was junk.  Here I am not using it to produce "serious" data (as in a
    //	real simulation), only wall/partition location and some color
    srand((unsigned int) time(NULL));

    //	generate a random exit
    exitPos = getNewFreePosition();
    grid[exitPos.row][exitPos.col] = EXIT;

    //	Generate walls and partitions
    generateWalls();
    generatePartitions();

    //	Initialize traveler info structs
    //	You will probably need to replace/complete this as you add thread-related data
    float** travelerColor = createTravelerColors(numTravelers);
    for (unsigned int k=0; k<numTravelers; k++) {
        GridPosition pos = getNewFreePosition();
        Direction dir = static_cast<Direction>(rand() % NUM_DIRECTIONS);
        TravelerSegment seg = {pos.row, pos.col, dir};
        Traveler traveler;
        traveler.segmentList.push_back(seg);
        grid[pos.row][pos.col] = TRAVELER;

        //	I add 0-6 segments to my travelers
        unsigned int numAddSegments = rand() % 7;
        TravelerSegment currSeg = traveler.segmentList[0];
        bool canAddSegment = true;
        for (unsigned int s=0; s<numAddSegments && canAddSegment; s++)
        {
            TravelerSegment newSeg = newTravelerSeg(currSeg, canAddSegment);
            if (canAddSegment)
            {
                traveler.segmentList.push_back(newSeg);
                currSeg = newSeg;
            }
        }

        for (unsigned int c=0; c<4; c++)
            traveler.rgba[c] = travelerColor[k][c];

        travelerList.push_back(traveler);
    }

    //	free array of colors
    for (unsigned int k=0; k<numTravelers; k++)
        delete []travelerColor[k];
    delete []travelerColor;

    // create threads
    for (unsigned int i = 0; i < numTravelers; i++)
    {
		travelerList[i].index = i;
        ThreadInfo currentThread;
        currentThread.index = i;
        currentThread.stuck = false;
        currentThread.active = "RUNNING";
        // mutex lock to control access to traveler segment locations
        pthread_mutex_init(&currentThread.travelerLock, NULL);
        threadInfo.push_back(currentThread);
        int err = pthread_create(&threadInfo[i].threadID, NULL, findExit, &threadInfo[i]);
        if(err != 0)
        {
            printf("Could not create Thread %d.:\n", i);
            exit(EXIT_FAILURE);
        }
    }

}


//------------------------------------------------------
#if 0
#pragma mark -
#pragma mark Generation Helper Functions
#endif
//------------------------------------------------------

GridPosition getNewFreePosition(void)
{
    GridPosition pos;

    bool noGoodPos = true;
    while (noGoodPos)
    {
        unsigned int row = rand() % numRows;
        unsigned int col = rand() % numCols;
        if (grid[row][col] == FREE_SQUARE)
        {
            pos.row = row;
            pos.col = col;
            noGoodPos = false;
        }
    }
    return pos;
}

Direction newDirection(Direction forbiddenDir)
{
    bool noDir = true;

    Direction dir = NUM_DIRECTIONS;
    while (noDir)
    {
        dir = static_cast<Direction>(rand() % NUM_DIRECTIONS);
        noDir = (dir==forbiddenDir);
    }
    return dir;
}


TravelerSegment newTravelerSeg(const TravelerSegment& currentSeg, bool& canAdd)
{
    TravelerSegment newSeg;
    switch (currentSeg.dir)
    {
        case NORTH:
            if (	currentSeg.row < numRows-1 &&
                    grid[currentSeg.row+1][currentSeg.col] == FREE_SQUARE)
            {
                newSeg.row = currentSeg.row+1;
                newSeg.col = currentSeg.col;
                newSeg.dir = newDirection(SOUTH);
                grid[newSeg.row][newSeg.col] = TRAVELER;
                canAdd = true;
            }
                //	no more segment
            else
                canAdd = false;
            break;

        case SOUTH:
            if (	currentSeg.row > 0 &&
                    grid[currentSeg.row-1][currentSeg.col] == FREE_SQUARE)
            {
                newSeg.row = currentSeg.row-1;
                newSeg.col = currentSeg.col;
                newSeg.dir = newDirection(NORTH);
                grid[newSeg.row][newSeg.col] = TRAVELER;
                canAdd = true;
            }
                //	no more segment
            else
                canAdd = false;
            break;

        case WEST:
            if (	currentSeg.col < numCols-1 &&
                    grid[currentSeg.row][currentSeg.col+1] == FREE_SQUARE)
            {
                newSeg.row = currentSeg.row;
                newSeg.col = currentSeg.col+1;
                newSeg.dir = newDirection(EAST);
                grid[newSeg.row][newSeg.col] = TRAVELER;
                canAdd = true;
            }
                //	no more segment
            else
                canAdd = false;
            break;

        case EAST:
            if (	currentSeg.col > 0 &&
                    grid[currentSeg.row][currentSeg.col-1] == FREE_SQUARE)
            {
                newSeg.row = currentSeg.row;
                newSeg.col = currentSeg.col-1;
                newSeg.dir = newDirection(WEST);
                grid[newSeg.row][newSeg.col] = TRAVELER;
                canAdd = true;
            }
                //	no more segment
            else
                canAdd = false;
            break;

        default:
            canAdd = false;
    }

    return newSeg;
}

void generateWalls(void)
{
    const unsigned int NUM_WALLS = (numCols+numRows)/4;

    //	I decide that a wall length  cannot be less than 3  and not more than
    //	1/4 the grid dimension in its orientation
    const unsigned int MIN_WALL_LENGTH = 3;
    const unsigned int MAX_HORIZ_WALL_LENGTH = numCols / 3;
    const unsigned int MAX_VERT_WALL_LENGTH = numRows / 3;
    const unsigned int MAX_NUM_TRIES = 20;

    bool goodWall = true;

    //	Generate the vertical walls
    for (unsigned int w=0; w< NUM_WALLS; w++)
    {
        goodWall = false;

        //	Case of a vertical wall
        if (rand() %2)
        {
            //	I try a few times before giving up
            for (unsigned int k=0; k<MAX_NUM_TRIES && !goodWall; k++)
            {
                //	let's be hopeful
                goodWall = true;

                //	select a column index
                unsigned int HSP = numCols/(NUM_WALLS/2+1);
                unsigned int col = (1+ rand()%(NUM_WALLS/2-1))*HSP;
                unsigned int length = MIN_WALL_LENGTH + rand()%(MAX_VERT_WALL_LENGTH-MIN_WALL_LENGTH+1);

                //	now a random start row
                unsigned int startRow = rand()%(numRows-length);
                for (unsigned int row=startRow, i=0; i<length && goodWall; i++, row++)
                {
                    if (grid[row][col] != FREE_SQUARE)
                        goodWall = false;
                }

                //	if the wall first, add it to the grid
                if (goodWall)
                {
                    for (unsigned int row=startRow, i=0; i<length && goodWall; i++, row++)
                    {
                        grid[row][col] = WALL;
                    }
                }
            }
        }
            // case of a horizontal wall
        else
        {
            goodWall = false;

            //	I try a few times before giving up
            for (unsigned int k=0; k<MAX_NUM_TRIES && !goodWall; k++)
            {
                //	let's be hopeful
                goodWall = true;

                //	select a column index
                unsigned int VSP = numRows/(NUM_WALLS/2+1);
                unsigned int row = (1+ rand()%(NUM_WALLS/2-1))*VSP;
                unsigned int length = MIN_WALL_LENGTH + rand()%(MAX_HORIZ_WALL_LENGTH-MIN_WALL_LENGTH+1);

                //	now a random start row
                unsigned int startCol = rand()%(numCols-length);
                for (unsigned int col=startCol, i=0; i<length && goodWall; i++, col++)
                {
                    if (grid[row][col] != FREE_SQUARE)
                        goodWall = false;
                }

                //	if the wall first, add it to the grid
                if (goodWall)
                {
                    for (unsigned int col=startCol, i=0; i<length && goodWall; i++, col++)
                    {
                        grid[row][col] = WALL;
                    }
                }
            }
        }
    }
}


void generatePartitions(void)
{
    const unsigned int NUM_PARTS = (numCols+numRows)/4;

    //	I decide that a partition length  cannot be less than 3  and not more than
    //	1/4 the grid dimension in its orientation
    const unsigned int MIN_PART_LENGTH = 3;
    const unsigned int MAX_HORIZ_PART_LENGTH = numCols / 3;
    const unsigned int MAX_VERT_PART_LENGTH = numRows / 3;
    const unsigned int MAX_NUM_TRIES = 20;

    bool goodPart = true;

    for (unsigned int w=0; w< NUM_PARTS; w++)
    {
        goodPart = false;

        //	Case of a vertical partition
        if (rand() %2)
        {
            //	I try a few times before giving up
            for (unsigned int k=0; k<MAX_NUM_TRIES && !goodPart; k++)
            {
                //	let's be hopeful
                goodPart = true;

                //	select a column index
                unsigned int HSP = numCols/(NUM_PARTS/2+1);
                unsigned int col = (1+ rand()%(NUM_PARTS/2-2))*HSP + HSP/2;
                unsigned int length = MIN_PART_LENGTH + rand()%(MAX_VERT_PART_LENGTH-MIN_PART_LENGTH+1);

                //	now a random start row
                unsigned int startRow = rand()%(numRows-length);
                for (unsigned int row=startRow, i=0; i<length && goodPart; i++, row++)
                {
                    if (grid[row][col] != FREE_SQUARE)
                        goodPart = false;
                }

                //	if the partition is possible,
                if (goodPart)
                {
                    //	add it to the grid and to the partition list
                    SlidingPartition part;
                    part.isVertical = true;
                    for (unsigned int row=startRow, i=0; i<length && goodPart; i++, row++)
                    {
                        grid[row][col] = VERTICAL_PARTITION;
                        GridPosition pos = {row, col};
                        part.blockList.push_back(pos);
                    }
                    partitionList.push_back(part);
                }
            }
        }
            // case of a horizontal partition
        else
        {
            goodPart = false;

            //	I try a few times before giving up
            for (unsigned int k=0; k<MAX_NUM_TRIES && !goodPart; k++)
            {
                //	let's be hopeful
                goodPart = true;

                //	select a column index
                unsigned int VSP = numRows/(NUM_PARTS/2+1);
                unsigned int row = (1+ rand()%(NUM_PARTS/2-2))*VSP + VSP/2;
                unsigned int length = MIN_PART_LENGTH + rand()%(MAX_HORIZ_PART_LENGTH-MIN_PART_LENGTH+1);

                //	now a random start row
                unsigned int startCol = rand()%(numCols-length);
                for (unsigned int col=startCol, i=0; i<length && goodPart; i++, col++)
                {
                    if (grid[row][col] != FREE_SQUARE)
                        goodPart = false;
                }

                //	if the wall first, add it to the grid and build SlidingPartition object
                if (goodPart)
                {
                    SlidingPartition part;
                    part.isVertical = false;
                    for (unsigned int col=startCol, i=0; i<length && goodPart; i++, col++)
                    {
                        grid[row][col] = HORIZONTAL_PARTITION;
                        GridPosition pos = {row, col};
                        part.blockList.push_back(pos);
                    }
                    partitionList.push_back(part);
                }
            }
        }
    }
}


// returns index of move location closest to exit
int getOptimalLocation(std::vector<std::vector<unsigned int>>& potentialMoves, std::vector<std::vector<std::vector<int>>>& visitedLocations)
{
    // if only one potential move
    if (potentialMoves.size() == 1)
        return 0;

    // for each potential move
    for (unsigned i = 0; i < potentialMoves.size(); i++) {
        unsigned int row = potentialMoves[i][0];
        unsigned int col = potentialMoves[i][1];
        // if at least one move location not visited
        if (visitedLocations[row][col].empty()) {
            // remove all visited locations
            potentialMoves.erase(std::remove_if(potentialMoves.begin(), potentialMoves.end(),
                                                [&](auto &move) { return !visitedLocations[move[0]][move[1]].empty(); }), potentialMoves.end());
            // next move is to the location closest to the exit
            return std::min_element(begin(potentialMoves), end(potentialMoves),
                    [](auto &a, auto &b) { return a[2] < b[2]; }) - potentialMoves.begin();
        }
    }
    // all potential move locations are visited
    return -1;
}


// return index of next move location
int getNextLocation(std::vector<std::vector<unsigned int>>& potentialMoves, std::vector<std::vector<std::vector<int>>>& visitedLocations)
{
    // index of move location closest to exit
    int nextLocation = getOptimalLocation(potentialMoves, visitedLocations);

    // if all potential move locations are visited
    if (nextLocation == -1)
    {
        // number of times least traveled to location has been traveled to
        int minTravelCount;
        // choose the next location that is least visited
        // for each potential move
        for (unsigned int move = 0; move < potentialMoves.size(); move++)
        {
            // number of times move has been traveled to from current location
            int repeatedMoveCount = 0;
            // for each direction previously traveled
            for (unsigned int direction : visitedLocations[potentialMoves[move][0]][potentialMoves[move][1]])
            {
                // count the times this move was traveled to from the current location
                if (direction == potentialMoves[move][3])
                    repeatedMoveCount++;
            }
            // save the move least traveled to
            if (repeatedMoveCount < minTravelCount || move == 0)
            {
                nextLocation = move;
                minTravelCount = repeatedMoveCount;
            }
        }
    }

    return nextLocation;
}


// given the index of a partition in partitionList, partition move direction, and the distance the partition must be moved,
// returns true if partition can move out of the way for a traveler
bool validPartitionMove(int p, Direction dir, unsigned int moveDistance)
{
	GridPosition startLocation;
	int rowAdjust = 0; // adjust start location to always traverse grid from least to greatest values
	int colAdjust = 0;
	int rowIncrement;
	int colIncrement;
	int* i; // pointer to increment variable
	switch (dir) {
		case NORTH:
			startLocation = partitionList[p].blockList[0]; // potential partition end location
			rowAdjust = moveDistance+1;
			if((int)startLocation.row - rowAdjust < 0) // if partition end location is out of grid bounds
				return false;
			rowIncrement = 1;
			colIncrement = 0;
			i = &rowIncrement;
			break;
		case EAST:
			startLocation = partitionList[p].blockList.back(); // first partition block to move
			if(startLocation.col + moveDistance >= numCols) // if partition end location is out of grid bounds
				return false;
			rowIncrement = 0;
			colIncrement = 1;
			i = &colIncrement;
			break;
		case SOUTH:
			startLocation = partitionList[p].blockList.back(); // first partition block to move
			if(startLocation.row + moveDistance >= numRows) // if partition end location is out of grid bounds
				return false;
			rowIncrement = 1;
			colIncrement = 0;
			i = &rowIncrement;
			break;
		case WEST:
			startLocation = partitionList[p].blockList[0]; // potential partition end location
			colAdjust = moveDistance+1;
			if((int)startLocation.col - colAdjust < 0) // if partition end location is out of grid bounds
				return false;
			rowIncrement = 0;
			colIncrement = 1;
			i = &colIncrement;
			break;
	}

	// check if partition move is valid
	while (*i <= moveDistance)
	{
		// if next partition location is a wall or other partition
		if (grid[startLocation.row+rowIncrement-rowAdjust][startLocation.col+colIncrement-colAdjust] != FREE_SQUARE &&
		    grid[startLocation.row+rowIncrement-rowAdjust][startLocation.col+colIncrement-colAdjust] != TRAVELER)
		{
            //usleep(100000);
		    //std::cout<<"failed"<<*i<<std::endl;
			return false;
		}
		(*i)++;
	}

	// valid partition move
	return true;
}


// saves partition move to potentialMoves if move is possible
void savePartitionMoveIfValid(unsigned int currentRow, int row, unsigned int currentCol, int col,
        std::vector<std::vector<unsigned int>> &potentialMoves, unsigned int direction)
{
	// distance to move partition so traveler can move to obstructed location
    unsigned int northOrWestDistance = 0;
    unsigned int southOrEastDistance = 0;
    // direction to move the partition
    Direction northOrWestDirection;
    Direction southOrEastDirection;
    unsigned int p;
    // find partition in partitionList
    for (p = 0; p < partitionList.size(); p++)
    {
        for (unsigned int blockIndex = 0; blockIndex < partitionList[p].blockList.size(); blockIndex++)
        {
            // if partition found
            if (partitionList[p].blockList[blockIndex].row == currentRow+row
             && partitionList[p].blockList[blockIndex].col == currentCol+col)
            {
            	// distance partition has to move
	            northOrWestDistance = (partitionList[p].blockList.size() - blockIndex);
	            southOrEastDistance = (blockIndex+1);

	            // vertical partition
            	if (partitionList[p].isVertical)
                {
	                northOrWestDirection = NORTH;
	                southOrEastDirection = SOUTH;
	                if (!validPartitionMove(p, NORTH, northOrWestDistance))
		                northOrWestDistance = 0;
	                if (!validPartitionMove(p, SOUTH, southOrEastDistance))
		                southOrEastDistance = 0;

               }// horizontal partition
               else
               	{
	                southOrEastDirection = EAST;
	                northOrWestDirection = WEST;
	                if (!validPartitionMove(p, EAST, southOrEastDistance))
		                southOrEastDistance = 0;
	                if (!validPartitionMove(p, WEST, northOrWestDistance))
		                northOrWestDistance = 0;
	            }
               goto saveMove;
            }
        }
    }
    saveMove:;

    // if move location is invalid
    if ((northOrWestDistance == 0) && (southOrEastDistance == 0))
        return;

    // else save potential move with least partition move distance
    int rowDistance = currentRow + row - exitPos.row;
    int colDistance = currentCol + col - exitPos.col;
    if (northOrWestDistance < southOrEastDistance && northOrWestDistance != 0)
        potentialMoves.push_back({ currentRow + row, currentCol + col, (unsigned int)abs(rowDistance)+abs(colDistance), direction, p,
								   northOrWestDirection, northOrWestDistance, partitionList[p].blockList[0].row, partitionList[p].blockList[0].col });
    else if (southOrEastDistance != 0)
        potentialMoves.push_back({ currentRow + row, currentCol + col, (unsigned int)abs(rowDistance)+abs(colDistance), direction, p,
								   southOrEastDirection, southOrEastDistance, partitionList[p].blockList[0].row, partitionList[p].blockList[0].col });
}


// return true if partition moved successfully, return false if partition move is blocked by another partition
bool movePartition(std::vector<unsigned int>& move, unsigned int index)
{
	pthread_mutex_unlock(&threadInfo[index].travelerLock);
	std::cout<<"5: Thread "<<index <<" unlocked"<<std::endl;
	unsigned int p = move[4];
	Direction dir = static_cast<Direction>(move[5]);
	unsigned int moveDistance = move[6];
	SquareType partitionType;
	if (partitionList[p].isVertical)
		partitionType = VERTICAL_PARTITION;
	else
		partitionType = HORIZONTAL_PARTITION;

	// first partition block to move
	GridPosition firstBlockMove;
	// amount to adjust partition blocks by
	int rowIncrement;
	int colIncrement;
	int* i; // pointer to increment variable
	switch (dir) {
		case NORTH:
			firstBlockMove = partitionList[p].blockList[0];
			rowIncrement = -1;
			colIncrement = 0;
			i = &rowIncrement;
			break;
		case EAST:
			firstBlockMove = partitionList[p].blockList.back();
			rowIncrement = 0;
			colIncrement = 1;
			i = &colIncrement;
			break;
		case SOUTH:
			firstBlockMove = partitionList[p].blockList.back();
			rowIncrement = 1;
			colIncrement = 0;
			i = &rowIncrement;
			break;
		case WEST:
			firstBlockMove = partitionList[p].blockList[0];
			rowIncrement = 0;
			colIncrement = -1;
			i = &colIncrement;
			break;
	}

	// increment/decrementer
	int rowAdjust = rowIncrement;
	int colAdjust = colIncrement;
	std::function<int(int, int)> iterator;
	// increment counter
	if (dir == NORTH || dir == WEST)
		iterator = std::greater_equal<int>();
	else
		iterator = std::less_equal<int>();

	// move partition
	while (iterator(*i, moveDistance))
	{
		pthread_mutex_lock(&rowLocks[firstBlockMove.row+rowIncrement]);
		pthread_mutex_lock(&colLocks[firstBlockMove.col+colIncrement]);
		// if next partition location is another partition
		if (firstBlockMove.row+rowIncrement >= numRows || firstBlockMove.col+colIncrement >= numCols
		|| (grid[firstBlockMove.row+rowIncrement][firstBlockMove.col+colIncrement] != FREE_SQUARE &&
		    grid[firstBlockMove.row+rowIncrement][firstBlockMove.col+colIncrement] != TRAVELER))
		{
			pthread_mutex_unlock(&rowLocks[firstBlockMove.row+rowIncrement]);
			pthread_mutex_unlock(&colLocks[firstBlockMove.col+colIncrement]);
			return false;
		}

		// if next partition location is occupied by a segment
		if (grid[firstBlockMove.row+rowIncrement][firstBlockMove.col+colIncrement] == TRAVELER)
		{
			pthread_mutex_lock(&threadInfo[index].travelerLock);
			std::cout<<"6: Thread "<<index <<" locked"<<std::endl;
			for (TravelerSegment &seg : travelerList[index].segmentList)
			{
				// if travelers own segment is blocking partition
				if (seg.row == firstBlockMove.row + rowIncrement && seg.col == firstBlockMove.col + colIncrement)
				{
					pthread_mutex_unlock(&rowLocks[firstBlockMove.row+rowIncrement]);
					pthread_mutex_unlock(&colLocks[firstBlockMove.col+colIncrement]);
					pthread_mutex_unlock(&threadInfo[index].travelerLock);
					std::cout<<"7: Thread "<<index <<" unlocked"<<std::endl;
					return false;
				}
			}
			pthread_mutex_unlock(&threadInfo[index].travelerLock);
			std::cout<<"8: Thread "<<index <<" unlocked"<<std::endl;

			waitForFree:;
			// wait for the traveler to move away
			pthread_mutex_unlock(&gridLock);
			std::cout<<"1: Grid unlocked " << index <<std::endl;
			while (grid[firstBlockMove.row + rowIncrement][firstBlockMove.col + colIncrement] == TRAVELER)
			{
				// check if traveler is stuck
				for (Traveler& t : travelerList)
				{
					pthread_mutex_lock(&threadInfo[t.index].travelerLock);
					std::cout<<"8b: Thread "<<t.index <<" locked"<<std::endl;
					for (TravelerSegment& seg : t.segmentList)
					{
						// if found occupying traveler segment
						if (seg.row == firstBlockMove.row+rowIncrement && seg.col == firstBlockMove.col+colIncrement)
						{
							pthread_mutex_unlock(&threadInfo[index].travelerLock);
							std::cout<<"8c: Thread "<<index <<" unlocked"<<std::endl;
							// if the occupying traveler is stuck
							if (threadInfo[t.index].stuck || !running)
							{
								pthread_mutex_unlock(&rowLocks[firstBlockMove.row+rowIncrement]);
								pthread_mutex_unlock(&colLocks[firstBlockMove.col+colIncrement]);
								return false;
							}
							else
								goto checkIfMoved;
						}
					}
					pthread_mutex_unlock(&threadInfo[t.index].travelerLock);
					std::cout<<"8d: Thread "<<t.index <<" unlocked"<<std::endl;
				}
				checkIfMoved:;
				usleep(travelerSleepTime);
			}
			pthread_mutex_lock(&gridLock);
			std::cout<<"2: Grid locked " << index <<std::endl;
			// check if still free after gridLock is acquired
			if (grid[firstBlockMove.row + rowIncrement][firstBlockMove.col + colIncrement] == TRAVELER)
				goto waitForFree;
		}

		// assign old partition location grid type
		if (dir == NORTH || dir == WEST)
			grid[partitionList[p].blockList.back().row][partitionList[p].blockList.back().col] = FREE_SQUARE;
		else
			grid[partitionList[p].blockList[0].row][partitionList[p].blockList[0].col] = FREE_SQUARE;
		// assign new partition location grid type
		grid[firstBlockMove.row+rowIncrement][firstBlockMove.col+colIncrement] = partitionType;

		// move partition 1 block
		for (int b = 0; b < partitionList[p].blockList.size(); b++)
		{
			partitionList[p].blockList[b].row += rowAdjust;
			partitionList[p].blockList[b].col += colAdjust;
		}
		// increment counter
		if (dir == NORTH || dir == WEST)
			(*i)--;
		else
			(*i)++;
		usleep(travelerSleepTime);
	}

	pthread_mutex_unlock(&rowLocks[firstBlockMove.row+rowIncrement]);
	pthread_mutex_unlock(&colLocks[firstBlockMove.col+colIncrement]);
	// partition move successful
	return true;
}


// moves travelers (threads) to the exit point
void* findExit(void *currentThreadInfo)
{
    int moveCounter = 0; // number of moves traveler has made
    bool trapped = false; // true when traveler is permanently stuck
	numLiveThreads++; // increment number of live threads
    ThreadInfo* info = static_cast<ThreadInfo*>(currentThreadInfo); // cast argument
    // grid that stores the direction traveled to each location
    std::vector<std::vector<std::vector<int>>> visitedLocations(numRows, std::vector<std::vector<int>>(numCols));
	pthread_mutex_lock(&threadInfo[info->index].travelerLock);
	std::cout<<"9: Thread "<<info->index <<" locked"<<std::endl;
	// while not at exit
    while (grid[travelerList[info->index].segmentList[0].row][travelerList[info->index].segmentList[0].col] != EXIT)
    {
	    pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
	    std::cout<<"10: Thread "<<info->index <<" unlocked"<<std::endl;
	    moveCounter++;
        // add segment
        if (moveCounter%numMoves == 0)
        {
            bool canAddSegment = true;
	        pthread_mutex_lock(&gridLock);
	        std::cout<<"3: Grid locked " << info->index <<std::endl;
	        pthread_mutex_lock(&threadInfo[info->index].travelerLock);
	        std::cout<<"11: Thread "<<info->index <<" locked"<<std::endl;
	        TravelerSegment newSeg = newTravelerSeg(travelerList[info->index].segmentList.back(), canAddSegment);
            travelerList[info->index].segmentList.push_back(newSeg);
	        pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
            std::cout<<"12: Thread "<<info->index << " unlocked" << std::endl;
	        pthread_mutex_unlock(&gridLock);
	        std::cout<<"4: Grid unlocked " << info->index <<std::endl;

        }

        // if application is killed
        if (!running)
        {
	        threadInfo[info->index].active == "DONE";
	        return NULL;
        }

        usleep(travelerSleepTime);
	    pthread_mutex_lock(&threadInfo[info->index].travelerLock);
        std::cout<<"13: Thread "<<info->index <<" locked"<<std::endl;
	    unsigned int currentRow = travelerList[info->index].segmentList[0].row;
        unsigned int currentCol = travelerList[info->index].segmentList[0].col;
	    pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
        std::cout<<"14: Thread "<<info->index  << " unlocked" << std::endl;
	    /* { row, col, distance from exit, direction to travel to this location, (also for moves occupied by partitions:)
		 *   partition index, partition move direction, distance the partition must be moved, partition start location row, partition start location col } */
        std::vector<std::vector<unsigned int>> potentialMoves;
        std::vector<std::vector<unsigned int>> occupiedMoves;
        // {row, col}
        std::vector<std::pair<int, int>> moves = { {0, -1}, {1, 0}, {0, 1}, {-1, 0} };
        pthread_mutex_lock(&gridLock);
	    std::cout<<"5: Grid locked " << info->index <<std::endl;
	    // for 4 potential moves
        for (unsigned i = 0; i < moves.size(); i++)
        {
            // location of potential move
            int row = moves[i].first;
            int col = moves[i].second;
            // direction value: 1 west, 2 south, 3 east, 4 north
            unsigned int direction = i + 1;

            // if move is (in grid bounds AND a free square)
            if (currentRow+row < numRows && currentCol+col < numCols)
            {
                // if move is at free square or exit
                if (grid[currentRow + row][currentCol + col] == FREE_SQUARE || grid[currentRow + row][currentCol + col] == EXIT) {
                    // save potential move and its distance from exit
                    int rowDistance = currentRow + row - exitPos.row;
                    int colDistance = currentCol + col - exitPos.col;
                    potentialMoves.push_back({ currentRow + row, currentCol + col, (unsigned int)abs(rowDistance)+abs(colDistance), direction });
                }
                // if move is at partition and location behind partition is a valid move
                else if (grid[currentRow + row][currentCol + col] == VERTICAL_PARTITION || grid[currentRow + row][currentCol + col] == HORIZONTAL_PARTITION)
                {
                    savePartitionMoveIfValid(currentRow, row, currentCol, col, potentialMoves, direction);
                }
                // collect moves occupied by another traveler or own segment
                else if (grid[currentRow + row][currentCol + col] == TRAVELER)
                {
                    // save potential move and its distance from exit
                    int rowDistance = currentRow + row - exitPos.row;
                    int colDistance = currentCol + col - exitPos.col;
                    occupiedMoves.push_back({ currentRow + row, currentCol + col, (unsigned int)abs(rowDistance)+abs(colDistance), direction });
                }
            }
        }

        // index of next move location in potentialMoves
        int nextLocation;
        // if stuck
        if (potentialMoves.empty())
        {
	        pthread_mutex_unlock(&gridLock);
	        std::cout<<"14g: Grid unlocked " << info->index <<std::endl;
	        // remove move locations that will never be available
            occupiedMoves.erase(std::remove_if(occupiedMoves.begin(), occupiedMoves.end(), [info](auto const& move)
            {
	            pthread_mutex_lock(&threadInfo[info->index].travelerLock);
                std::cout<<"15: Thread "<<info->index <<" locked"<<std::endl;
	            // remove move locations that are occupied by current traveler's segment
                for (auto const& segment : travelerList[info->index].segmentList)
                {
                    // if the segment that is occupying move location
                    if (segment.row == move[0] && segment.col == move[1])
                    {
	                    pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
	                    return true;
                    }
                }
	            pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
                std::cout<<"16: Thread "<<info->index  << " unlocked" << std::endl;

	            // remove locations occupied by travelers' who have a status of stuck
                for (unsigned int t = 0; t < travelerList.size(); t++)
                {
                	pthread_mutex_lock(&threadInfo[t].travelerLock);
	                std::cout << "17: Thread " << info->index << " locked" << std::endl;
	                // if adjacent traveler is also stuck
                    if (threadInfo[t].stuck)
                    {
	                    pthread_mutex_unlock(&threadInfo[t].travelerLock);
	                    std::cout << "18: Thread " << info->index << " unlocked " << " thread " << t << std::endl;
	                    return true;
                    }
	                pthread_mutex_unlock(&threadInfo[t].travelerLock);
	                std::cout << "18b: Thread " << info->index << " unlocked" << " thread " << t << std::endl;
                }
                return false;
            }), occupiedMoves.end());

	        // if permanently stuck (trapped by traveler segments and/or barriers), kill thread
            if (occupiedMoves.empty() || !running)
            {
                trapped = true;
	            pthread_mutex_lock(&threadInfo[info->index].travelerLock);
                std::cout<<"19: Thread "<<threadInfo[info->index].threadID<<" locked"<<std::endl;
	            std::cout << "Traveler " << info->index << " got trapped and was terminated." << std::endl;
                // free head location and hide head at exit location
                grid[travelerList[info->index].segmentList[0].row][travelerList[info->index].segmentList[0].col] = FREE_SQUARE;
                travelerList[info->index].segmentList[0].row = exitPos.row;
                travelerList[info->index].segmentList[0].col = exitPos.col;
	            pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
                std::cout<<"20: Thread "<< info->index << " unlocked" << std::endl;
	            break;
            }

            // wait for an adjacent traveler to move away
	        std::cout<<"7: Grid locked " << info->index <<std::endl;
            info->stuck = true;
            while (info->stuck)
            {
                for (unsigned int m = 0; m < occupiedMoves.size(); m++)
                {
                    pthread_mutex_lock(&gridLock);
	                std::cout<<"8: Grid locked " << info->index <<std::endl;
                    // if a move location is now available
                    if (grid[occupiedMoves[m][0]][occupiedMoves[m][1]] == FREE_SQUARE || !running) {
                        pthread_mutex_unlock(&gridLock);
	                    std::cout<<"9: Grid unlocked " << info->index <<std::endl;
                        potentialMoves = {occupiedMoves[m]};
                        nextLocation = 0;
                        info->stuck = false;
                        break;
                    }
                    pthread_mutex_unlock(&gridLock);
	                std::cout<<"10: Grid unlocked " << info->index <<std::endl;
                    // check possible move locations again
                    if (m > 100 || !running)
                    {
                    	info->stuck = false;
	                    continue;
                    }
                    usleep(travelerSleepTime);
                }
            }
        }
        else
            {
                pthread_mutex_unlock(&gridLock);
	            std::cout<<"11: Grid unlocked " << info->index <<std::endl;
                // index of next move location
                nextLocation = getNextLocation(potentialMoves, visitedLocations);
            }

        pthread_mutex_lock(&gridLock);
	    std::cout<<"12: Grid locked " << info->index <<std::endl;
        // if move is occupied by a partition
        if (potentialMoves[nextLocation].size() == 9)
        {
	        std::vector<GridPosition>& blockList = partitionList[potentialMoves[nextLocation][4]].blockList;
	        std::vector<unsigned int>& savedPartition = potentialMoves[nextLocation];
        	// if partition has been moved by another traveler before gridLock was acquired
        	if (blockList[0].row != savedPartition[7] && blockList[0].col != savedPartition[8] || !running)
	        {
		        pthread_mutex_unlock(&gridLock);
		        std::cout<<"13: Grid unlocked " << info->index <<std::endl;
				continue;
	        }
	        pthread_mutex_lock(&threadInfo[info->index].travelerLock);
            std::cout<<"21: Thread "<<info->index <<" locked (1164)"<<std::endl;
	        // move partition, if move fails
            if(!movePartition(potentialMoves[nextLocation], info->index) || !running) {
	            pthread_mutex_unlock(&gridLock);
	            std::cout<<"14: Grid unlocked " << info->index <<std::endl;
	            continue;
            }
            // movePartition function unlocks traveler mutex

        }

        // set new move square to TRAVELER if not moving to exit
        if (grid[potentialMoves[nextLocation][0]][potentialMoves[nextLocation][1]] == FREE_SQUARE)
            grid[potentialMoves[nextLocation][0]][potentialMoves[nextLocation][1]] = TRAVELER;
        // if new move location is no longer available
        else if (grid[potentialMoves[nextLocation][0]][potentialMoves[nextLocation][1]] == TRAVELER || !running)
        {
            pthread_mutex_unlock(&gridLock);
	        std::cout<<"15: Grid unlocked " << info->index <<std::endl;
            continue;
        }

        // set last segment location to free
	    pthread_mutex_lock(&threadInfo[info->index].travelerLock);
        std::cout<<"23: Thread "<<info->index <<" locked"<<std::endl;
	    grid[travelerList[info->index].segmentList.back().row][travelerList[info->index].segmentList.back().col] = FREE_SQUARE;
        // set entire segment to the one before it
        for (int i = travelerList[info->index].segmentList.size()-1; i > 0; i--)
        {
            if (grid[travelerList[info->index].segmentList[i-1].row][travelerList[info->index].segmentList[i-1].col] != EXIT)
                travelerList[info->index].segmentList[i] = travelerList[info->index].segmentList[i-1];
            else
            {
                travelerList[info->index].segmentList[i].row = travelerList[info->index].segmentList[i-1].row;
                travelerList[info->index].segmentList[i].col = travelerList[info->index].segmentList[i-1].col;

            }

        }

        // mark previous location as visited, saving the direction traveled to it
        visitedLocations[currentRow][currentCol].push_back(static_cast<int>(travelerList[info->index].segmentList[0].dir));
        // assign new traveler direction and location
        travelerList[info->index].segmentList[0].dir = static_cast<Direction>((potentialMoves[nextLocation][3]) % NUM_DIRECTIONS);
        travelerList[info->index].segmentList[0].row = potentialMoves[nextLocation][0];
        travelerList[info->index].segmentList[0].col = potentialMoves[nextLocation][1];
	    pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
        std::cout<<"24: Thread "<< info->index << " unlocked" << std::endl;
	    pthread_mutex_unlock(&gridLock);
	    std::cout<<"16: Grid unlocked " << info->index <<std::endl;
    }

    pthread_mutex_lock(&gridLock);
	std::cout<<"17: Grid locked " << info->index <<std::endl;
	pthread_mutex_lock(&threadInfo[info->index].travelerLock);
    std::cout<<"25: Thread "<<info->index <<" locked"<<std::endl;
	// free tail segment locations once at exit or trapped
    for (unsigned int i = 1; i < travelerList[info->index].segmentList.size(); i++)
    {
        grid[travelerList[info->index].segmentList[i].row][travelerList[info->index].segmentList[i].col] = FREE_SQUARE;
    }
    // delete traveler and adjust counters
    travelerList[info->index].segmentList.clear();
    if (!trapped)
    	numTravelersDone++;
    numLiveThreads--;
    info->active = "DONE";
	pthread_mutex_unlock(&threadInfo[info->index].travelerLock);
    std::cout<<"26: Thread "<< info->index << " unlocked" << std::endl;
	pthread_mutex_unlock(&gridLock);
	std::cout<<"18: Grid unlocked " << info->index <<std::endl;
    return NULL;
}

